"""
WSGI config for MySQL_TryFix project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'MySQL_TryFix.settings')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'importexport.settings')  # youtube

application = get_wsgi_application()
