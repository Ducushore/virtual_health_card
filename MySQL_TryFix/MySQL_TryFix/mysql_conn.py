import MySQLdb
import openpyxl

wb = openpyxl.load_workbook(filename="file", read_only=True)
ws = wb['My Worksheet']

conn = MySQLdb.connect()
cursor = conn.cursor()

sql = """LOAD DATA INFILE 'data.xlsx' 
         INTO TABLE myTable  
         FIELDS TERMINATED BY ',' 
         OPTIONALLY ENCLOSED BY '\"'
         LINES TERMINATED BY '\n'"""

cursor.execute(sql)

for row in ws.iter_rows(row_offset=1):
    sql_row = 35000
    cursor.execute("INSERT sql_row")

conn.commit()
