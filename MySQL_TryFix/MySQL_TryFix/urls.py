"""MySQL_TryFix URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from card_app.views import doctor_personal_data_review, patient_personal_data_review, pharmacist_personal_data_review, \
    doctor_new_consultation, doctor_new_prescription

"""importexport URL Configuration
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
from django.contrib import admin
from django.urls import path
from projectapp import views
"""

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('card_app.urls')),
    # path('upload', views.simple_upload(request)),  # youtube / pe gitgub este "path('', views.simple_upload),"
    path('users/', include('django.contrib.auth.urls')),  # for setting up authentication view

    path('drpdr', doctor_personal_data_review),  # NEW (replacing: 'doctor_detail_view')
    path('papdr', patient_personal_data_review),  # NEW
    path('phpdr', pharmacist_personal_data_review),  # NEW
    path('drnewc', doctor_new_consultation),  # NEW
    path('drnewp', doctor_new_prescription),  # NEW
]
