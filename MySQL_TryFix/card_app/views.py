# https://docs.djangoproject.com/en/2.2/_modules/django/db/models/base/#Model.save

from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponse
from django.shortcuts import render
from django.urls import reverse

from .models import *
from django.conf import settings
import traceback
from django.http import Http404


def doctor_pages_access_redirected(request):  # NEW: 03.DEC
    if len(Pharmacist.objects.filter(user_id=request.user.id)) != 0:
        return pharmacist_personal_data_review(request)

    if len(Patient.objects.filter(user_id=request.user.id)) != 0:
        return patient_personal_data_review(request)


def pharmacist_pages_access_redirected(request):  # NEW: 03.DEC
    if len(Pharmacist.objects.filter(user_id=request.user.id)) == 0:  # NEW: 15.DEC
        return virtual_health_card(request)

    if len(Doctor.objects.filter(user_id=request.user.id)) != 0:
        return doctor_personal_data_review(request)

    if len(Patient.objects.filter(user_id=request.user.id)) != 0:
        return patient_personal_data_review(request)


def patient_pages_access_redirected(request):  # NEW: 03.DEC
    if len(Pharmacist.objects.filter(user_id=request.user.id)) != 0:
        return pharmacist_personal_data_review(request)

    if len(Doctor.objects.filter(user_id=request.user.id)) != 0:
        return doctor_personal_data_review(request)


def doctor_personal_data_review(request):  # NEW
    """            # NEW: 03.DEC :
    This is a view function. Each such function takes at least one parameter, called "request" by convention.
    This is an object that contains information about the current Web request that has triggered this view,
    and is an instance of the class 'django.http.HttpRequest'.
    """
    doctor_pages_access_redirected(request)  # NEW: 03.DEC

    # obj = Doctor.objects.get(user_id=request.user.id)  # in loc de "obj = Doctor.objects.get(id=1)"
    obj = Doctor.objects.filter(user_id=request.user.id)  # NEW: 10.DEC - a rezolvat eroarea urmatoare:
    # "DoesNotExist at /doctor_prescription_review . Doctor matching query does not exist."
    context = {
        'object': obj,
        'page_title': "DOCTOR's Personal Data Review",
        'main_title': "DOCTOR's Personal Data Review for ",
    }
    return render(request, 'doctor/doctor_personal_data_review.html', context)


# DOCTORS' VIEWS :
def doctor_patient_search(request):
    doctor_pages_access_redirected(request)  # NEW: 03.DEC

    context = {'patients': Patient.objects.all(),  # toti pacientii au fost trimisi catre front-end
               'prescriptions': Prescription.objects.all(),  # toate presciptions trimise catre front-end
               'page_title': "DOCTOR's Patient Search",
               'main_title': "DOCTOR's PATIENT-SEARCH Page",
               }
    # filtrarile pot fi facute pe front-end sau pe back-end
    # '.all()' furnizeaza toti pacientii, 'filter( . . .)' numai un anumit pacient, functie de conditie

    return render(request, 'doctor/doctor_patient_search.html', context)
    # {'form': context} instead of context


def doctor_all_consultations_review(request):
    doctor_pages_access_redirected(request)  # NEW: 03.DEC

    obj = Doctor.objects.get(user_id=request.user.id)  # in loc de "obj = Doctor.objects.get(id=1)"

    context = {
        'object': obj,
        'consultations': Consultation.objects.all(),
        'page_title': 'Consultation(s) Review',
        'main_title': 'ALL CONSULTATION(S) REVIEW Page',
    }
    return render(request, 'doctor/doctor_all_consultations_review.html', context)


def doctor_all_prescriptions_review(request):  # NEW: 10.DEC: from "doctor_prescription_review"
    doctor_pages_access_redirected(request)  # NEW: 03.DEC
    obj = Doctor.objects.filter(user_id=request.user.id)

    context = {
        'object': obj,
        'prescriptions': Prescription.objects.all(),  # filter(Prescription.patient.ssn)
        'page_title': "Prescription(s) Search",
        'main_title': "DOCTOR's PRESCRIPTION(S) Search by SSN(PATIENT) = ",
    }
    # row_count = Prescription.objects.filter(creation_date='2020-01-20', id=0).count()  # NEW/26.NOV.2020 {# TODO: NU SCOATE NIMIC . . #}
    # print(row_count)  # This will give you the count of the rows   # NEW/26.NOV.2020  {# TODO: NU SCOATE NIMIC . . #}
    return render(request, 'doctor/doctor_all_prescriptions_review.html', context)


def doctor_prescription_by_SSN_review(request):  # NEW: 10.DEC: from "doctor_prescription_review"
    doctor_pages_access_redirected(request)  # NEW: 03.DEC
    # obj = Doctor.objects.get(user_id=request.user.id)  # in loc de "obj = Doctor.objects.get(id=1)"
    # NU MERGE : "module 'django.db.models' has no attribute 'Prescription'"

    obj = Doctor.objects.filter(user_id=request.user.id)  # NEW: 08.DEC - a rezolvat eroarea urmatoare:
    # "DoesNotExist at /doctor_prescription_review . Doctor matching query does not exist."

    context = {
        'object': obj,
        'prescriptions': Prescription.objects.all(),  # => am sters "model." din fatza "Prescription.objects.all(),"
        'page_title': 'Prescription(s) Review',
        'main_title': 'PRESCRIPTION(S) REVIEW Page by SSN',
    }
    # row_count = Prescription.objects.filter(creation_date='2020-01-20', id=0).count()  # NEW/26.NOV.2020 {# TODO: NU SCOATE NIMIC . . #}
    # print(row_count)  # This will give you the count of the rows   # NEW/26.NOV.2020  {# TODO: NU SCOATE NIMIC . . #}
    return render(request, 'doctor/doctor_prescription_by_SSN_review.html', context)


def doctor_new_consultation(request):
    doctor_pages_access_redirected(request)  # NEW: 03.DEC

    current_doctor_con = Doctor.objects.get(user=request.user)

    context = {
        'page_title': 'New Consultation',
        'main_title': 'NEW CONSULTATION Page',
        'sub_title_1': "NEW CONSULTATION",
        'sub_title_2': "NEW CONSULTATION",
        'sub_title_3': "    => Fill-in all the necessary data and then press the SAVE button :",
    }
    if request.method == "POST":  # TODO: metoda asta se apeleaza cand userul apasa pe butonul de submit.
        print("This is the request.POST views.py, doctor_new_consultation, line 136 : ", request.POST)

        # 2. CLINIC:                                                # NEW 03.DEC
        clinic_id = request.POST.get('clinic_id', '')
        print("clinic_id (doctor_new_consultation)= ", clinic_id)
        clinic_type_id = request.POST.get('clinic_type_id', '')

        # 4. PACIENT:                                               # NEW 03.DEC
        patient_id = request.POST.get('patient_id', '')

        # 6. INSURANCE TYPE:                                        # NEW 03.DEC
        # insurance_id = request.POST.get('insurance_id', '')

        # 8. DIAGNOSTIC TYPE:                                       # NEW 03.DEC
        diagnostic_type_id = request.POST.get('diagnostic_type_id', '')

        # 9. MEDICAL REPORT:                                        # NEW 03.DEC
        medical_report = request.POST.get('medical_report', '')  # " , '' " reprezinta valoarea 'default'
        # medical_report = request.POST['medical_report']  # NU ESTE OK => . . .
        # . . . => " django.utils.datastructures.MultiValueDictKeyError: 'medical_report' "
        patient = Patient.objects.get(id=patient_id)
        insurance_id = patient.insurance.id

        Consultation.objects.create(
            doctor_id=current_doctor_con.id,
            clinic_id=clinic_id,
            clinic_type_id=clinic_type_id,

            patient_id=patient_id,
            insurance_id=insurance_id,
            diagnostic_type_id=diagnostic_type_id,

            medical_report=medical_report,
        )
        return HttpResponseRedirect('/doctor_all_consultations_review')

    if request.method == "GET":  # TODO: de obicei, metoda asta se apeleaza cand dai ENTER pe un URL in browser.
        # Attach existing user details if present, boxes should become greyed out
        print("This is the request.GET views.py, line 168 : ", request.GET)  # NEW: 01.DEC.2020

        context['current_doctor_con'] = current_doctor_con

        context['doctors'] = Doctor.objects.all()           # pentru 'doctor first and last names' drop-down list
        context['clinics'] = Clinic.objects.all()           # pentru 'Clinic' drop-down list
        context['clinic_types'] = ClinicType.objects.all()  # pentru 'Clinic Type' drop-down list

        context['patients'] = Patient.objects.all()
        context['insurances'] = Insurance.objects.all()             # pentru 'Insurance Type' drop-down list
        context['residences'] = Residence.objects.all()             # pentru 'Residence' drop-down list
        context['diagnostic_types'] = DiagnosticType.objects.all()  # pentru 'Diagnostic Type' drop-down list

        return render(request, 'doctor/doctor_new_consultation.html', context)


def doctor_new_prescription(request):
    doctor_pages_access_redirected(request)  # NEW: 03.DEC

    current_doctor_pre = Doctor.objects.get(user=request.user)          # NEW: 14.DEC

    context = {'page_title': 'New Prescription',
               'main_title': 'NEW PRESCRIPTION Page',  # 'NEW CONSULTATION and PRESCRIPTION Page'
               'sub_title_1': "NEW PRESCRIPTION",
               'sub_title_2': "NEW PRESCRIPTION",
               'sub_title_3': "    => Fill-in all the necessary data and then press the SAVE button :",
               }

    if request.method == "POST":  # TODO: metoda asta se apeleaza cand userul apasa pe butonul de submit.
        print("This is the request.POST views.py, doctor_new_prescription(request), line 197 : ", request.POST)

        # 1. Create PRESCRIPTION profile :                          # NEW: 14.DEC
        series = request.POST.get('series', '')
        number = request.POST.get('number', '')

        insurance_id = request.POST.get('insurance_id', '')  # 'insurance_id' is linked to template
        print("insurance_id (doctor_new_prescription - linia 210)= ", insurance_id)
        residence_id = request.POST.get('residence_id', '')
        print("residence_id (doctor_new_prescription - linia 212)= ", residence_id)

        # 2. Create DOCTOR profile :                                # NEW: 14.DEC
        authorization = request.POST.get('auth_no', '')
        doctor_type_id = request.POST.get('doctor_type_id', '')
        speciality_id = request.POST.get('speciality_id', '')

        # 3. CLINIC:                                                # NEW: 14.DEC
        clinic_id = request.POST.get('clinic_id', '')
        print("clinic_id (doctor_new_consultation)= ", clinic_id)
        clinic_type_id = request.POST.get('clinic_type_id', '')
        print("clinic_type_id (doctor_new_consultation)= ", clinic_type_id)

        # 2. PACIENT:                                               # NEW: 14.DEC
        patient_id = request.POST.get('patient_id', '')
        print("patient_id (doctor_new_consultation)= ", patient_id)

        # 3. INSURANCE TYPE:                                        # NEW: 14.DEC
        # insurance_id = request.POST.get('insurance_id', '')

        # 4. RESIDENCE:                                             # NEW: 14.DEC
        # residence_id = request.POST.get('residence_id', '')

        # 5. DIAGNOSTIC TYPE:                                       # NEW: 14.DEC
        diagnostic_type_id = request.POST.get('diagnostic_type_id', '')
        print("diagnostic_type_id (doctor_new_consultation)= ", diagnostic_type_id)

        # 6. DIAGNOSTIC CODE:                                       # NEW: 14.DEC
        diagnostic_code = request.POST.get('diagnostic_code', '')
        # prescription_type diagnostic_code

        # 7. PRESCRIPTION TYPE:                                     # NEW: 14.DEC
        prescription_type_id = request.POST.get('prescription_type_id', '')
        print("prescription_type_id (doctor_new_consultation)= ", prescription_type_id)
        # consultation diagnostic_type diag_type

        Prescription.objects.create(                                # NEW: 14.DEC
            series=series,
            number=number,

            doctor_id=current_doctor_pre.id,
            clinic_id=clinic_id,
            clinic_type_id=clinic_type_id,

            patient_id=patient_id,
            insurance_id=insurance_id,
            residence_id=residence_id,
            diagnostic_type_id=diagnostic_type_id,

            # diagnostic_code=diagnostic_code,
            prescription_type_id=prescription_type_id,
        )
        return HttpResponseRedirect('/doctor_all_prescriptions_review')

    if request.method == "GET":  # TODO: de obicei, metoda asta se apeleaza cand dai ENTER pe un URL in browser.
        # Attach existing user details if present, boxes should become greyed out
        print("This is the request.GET views.py, doctor_new_prescription(request), line 268 : ", request.GET)

        context['current_doctor_pre'] = current_doctor_pre

        context['doctors'] = Doctor.objects.all()           # pentru 'doctor first and last names' drop-down list
        context['clinics'] = Clinic.objects.all()           # pentru 'Clinic' drop-down list
        context['clinic_types'] = ClinicType.objects.all()  # pentru 'Clinic Type' drop-down list

        context['patients'] = Patient.objects.all()
        context['insurances'] = Insurance.objects.all()             # pentru 'Insurance Type' drop-down list
        context['residences'] = Residence.objects.all()             # pentru 'Residence' drop-down list
        context['diagnostic_types'] = DiagnosticType.objects.all()  # pentru 'Diagnostic Type' drop-down list

        context['prescriptiontypes'] = PrescriptionType.objects.all()  # pentru 'Diagnostic Code' drop-down list

        return render(request, 'doctor/doctor_new_prescription.html', context)


def doctor_registration(request):
    doctor_pages_access_redirected(request)  # NEW: 03.DEC

    context = {'page_title': 'New Doctor',
               'main_title': 'New DOCTOR......',
               'sub_title_1': "For registration, please fill-in the following fields :",
               # 'footer': "(*) These fields are mandatory !",  # eliminat - setare din baza de date
               }
    if request.method == "POST":  # TODO: metoda asta se apeleaza cand userul apasa pe butonul de submit.
        print("This is the request.POST views.py, doctor_registration, line 291 : ", request.POST)

        # 1. Create USER profile, if not exist :
        email = request.POST.get('email', '')
        first_name = request.POST.get('first_name', '')
        last_name = request.POST.get('last_name', '')
        password = request.POST.get('password', '')
        print("password, line 281 : ", password)

        existing_user = User.objects.filter(email=email)
        if len(existing_user) > 0:
            user = User.objects.get(email=email)
            print("user, line 303 : ", user)
        else:
            user = User.objects.create_user(
                first_name=first_name,
                last_name=last_name,
                username=email,
                password=password,
                email=email,
                is_active=True
            )
            print("user, line 313 : ", user)

        # 2. Create DOCTOR profile, if not exist :
        authorization = request.POST.get('auth_no', '')
        doctor_type_id = request.POST.get('doctor_type_id', '')     # "_id" este CORECT
        speciality_id = request.POST.get('speciality_id', '')       # "_id" este CORECT

        existing_doctor = Doctor.objects.filter(user=user)
        if len(existing_doctor) > 0:
            doctor = Doctor.objects.get(user=user)
            print("doctor, line 306 : ", doctor)
        else:
            doctor = Doctor.objects.create(
                user=user,
                authorisation=authorization,
                speciality_id=speciality_id,
                doctor_type_id=doctor_type_id,
            )     # "_id" este CORECT
            print("doctor (doctor_registration - linia 331)= ", doctor)

        # 3. CLINIC and CLINIC-TYPE :                             # NEW 03.DEC
        clinic_id = request.POST.get('clinic_id', '')
        print("clinic_id (doctor_registration - linia 335)= ", clinic_id)

        clinic_type_id = request.POST.get('clinic_type_id', '')
        print("clinic_type_id (doctor_registration - linia 338)= ", clinic_type_id)

        return HttpResponseRedirect('/')  # dupa salvarea datelor ne intoarcem in pagina HOME

    if request.method == "GET":  # TODO: de obicei, metoda asta se apeleaza cand dai ENTER pe un URL in browser.
        print('request.method == "GET" - linia 343', request.GET)

        context['specialities'] = Speciality.objects.all()
        context['doctor_types'] = DoctorType.objects.all()
        context['clinics'] = Clinic.objects.all()  # pentru 'Clinic' drop-down list
        context['clinic_types'] = ClinicType.objects.all()  # pentru 'Clinic Type' drop-down list

        return render(request, 'doctor/doctor_registration.html', context)


# PHARMACISTS' VIEWS :

def pharmacist_all_prescriptions_review(request):
    pharmacist_pages_access_redirected(request)
    obj = Pharmacist.objects.filter(user_id=request.user.id)

    context = {
        'object': obj,
        'prescriptions': Prescription.objects.all(),  # toate presciptions trimise catre front-end
        'page_title': "Prescription(s) Search",
        'main_title': "PHARMACIST's PRESCRIPTION(S) Search by SSN(PATIENT) = ",
    }
    # filtrarile pot fi facute pe front-end sau pe back-end
    # all furnizeaza toti pacientii, dar 'filter( . . .)' numai un anumit pacient, functie de conditie

    return render(request, 'pharmacist/pharmacist_all_prescriptions_review.html', context)
    # inlocuit "context" cu {'form': context} instead of context


def pharmacist_registration(request):
    pharmacist_pages_access_redirected(request)

    context = {'page_title': 'New Pharmacist',
               'main_title': 'New PHARMACIST......',
               'sub_title_1': "For registration, please fill-in the following fields :",
               # 'footer': "(*) These fields are mandatory !",  # eliminat - setare din baza de date
               }
    if request.method == "POST":  # TODO: metoda asta se apeleaza cand userul apasa pe butonul de submit.
        print("This is the request.POST views.py, pharmacist_registration, line 380 : ", request.POST)

        # 1. Create USER profile, if not exist :
        email = request.POST.get('email', '')
        first_name = request.POST.get('first_name', '')
        last_name = request.POST.get('last_name', '')
        password = request.POST.get('password', '')

        existing_user = User.objects.filter(email=email)
        if len(existing_user) > 0:
            user = User.objects.get(email=email)
        else:
            user = User.objects.create_user(
                first_name=first_name,
                last_name=last_name,
                username=email,
                password=password,
                email=email,
                is_active=True
            )

        # Create Pharmacist profile
        authorization = request.POST.get('auth_no', '')
        pharmacy = request.POST.get('pharmacy', '')

        existing_pharmacist = Pharmacist.objects.filter(user=user)
        if len(existing_pharmacist) > 0:
            pharmacist = Pharmacist.objects.get(user=user)
        else:
            pharmacist = Pharmacist.objects.create(
                user=user,
                authorisation=authorization,
                pharmacy_id=pharmacy
            )
        return HttpResponseRedirect('/')

    if request.method == "GET":  # TODO: de obicei, metoda asta se apeleaza cand dai ENTER pe un URL in browser.
        # Attach existing user details if present, boxes should become greyed out
        print(request.GET)
        context['pharmacies'] = Pharmacy.objects.all()
        return render(request, 'pharmacist/pharmacist_registration.html', context)


def pharmacist_personal_data_review(request):
    pharmacist_pages_access_redirected(request)

    obj = Pharmacist.objects.get(user_id=request.user.id)

    context = {
        'object': obj,
    }
    return render(request, 'pharmacist/pharmacist_personal_data_review.html', context)


# PACIENTS' VIEWS :

def patient_registration(request):
    patient_pages_access_redirected(request)

    context = {'page_title': 'New Patient',
               'main_title': 'New PATIENT......',
               'sub_title_1': "For registration, please fill-in the following fields :",
               # 'footer': "(*) These fields are mandatory !",  # eliminat - setare din baza de date
               }
    if request.method == "POST":  # TODO: metoda asta se apeleaza cand userul apasa pe butonul de submit.
        print("This is the request.POST views.py, patient_registration, line 434 : ", request.POST)

        # 1. Create USER profile, if not exist :
        email = request.POST.get('email', '')
        first_name = request.POST.get('first_name', '')
        last_name = request.POST.get('last_name', '')
        password = request.POST.get('password', '')

        existing_user = User.objects.filter(email=email)
        if len(existing_user) > 0:
            user = User.objects.get(email=email)
        else:
            user = User.objects.create_user(
                username=email,
                first_name=first_name,
                last_name=last_name,
                password=password,
                email=email,
                is_active=True,
            )

        # Create PATIENT profile :
        ssn = request.POST.get('ssn', '')
        phone_nr = request.POST.get('phone_no', '')
        date_of_birth = request.POST.get('date_of_birth', '')  #
        residence_id = request.POST.get('residence_id', '')
        insurance_id = request.POST.get('insurance_id', '')
        doctor_id = request.POST.get('doctor_id', '')

        existing_patient = Patient.objects.filter(user=user)
        if len(existing_patient) > 0:
            patient = Patient.objects.get(user=user)
        else:
            patient = Patient.objects.create(
                user=user,
                doctor_id=doctor_id,
                date_of_birth=date_of_birth,
                ssn=ssn,
                phone_nr=phone_nr,
                insurance_id=insurance_id,  # din "insurance_id=insurance" 07.DEC
                residence_id=residence_id,  # din "residence_id=residence" 07.DEC
            )
        return HttpResponseRedirect('/')

    if request.method == "GET":  # TODO: de obicei, metoda asta se apeleaza cand dai ENTER pe un URL in browser.
        # Attach existing user details if present, boxes should become greyed out
        print("This is the request.GET views.py, line 480 : ", request.GET)
        context['insurances'] = Insurance.objects.all()
        context['residences'] = Residence.objects.all()
        context['doctors'] = Doctor.objects.all()  # pentru 'doctor first and last names' drop-down list
        return render(request, 'patient/patient_registration.html', context)


def patient_personal_data_review(request):
    patient_pages_access_redirected(request)
    try:                    # NEW : 15.DEC -> catching an error and render it as a custom message :
        obj = Patient.objects.get(user_id=request.user.id)
    except Patient.DoesNotExist:
        raise Http404("Patient user IS NOT LOGGED-IN or DOES NOT EXIST yet.")
    
    context = {
        'object': obj,
    }
    return render(request, 'patient/patient_personal_data_review.html', context)


# HOME PAGE :

def virtual_health_card(request):
    context = {'page_title': 'Home',
               'main_title': 'VIRTUAL HEALTH CARD - Home Page',
               }
    return render(request, 'virtual_health_card.html', context)


# AUTHENTICATION VIEWS :

def logged_out(request):
    context = {'page_title': 'logged out',
               'main_title': 'Logged-Out......',
               }
    return render(request, 'virtual_health_card.html', context)


def password_reset_form(request):
    context = {'page_title': 'Password Reset Form',
               'main_title': 'Password Reset Form ......',
               'sub_title_1': "To reset the Password, please follow the steps :"
               }
    return render(request, 'registration/password_reset_form.html', context)


def password_reset_email(request):
    context = {'page_title': 'Password Reset email',
               'main_title': 'Password Reset email ......',
               }
    return render(request, 'registration/password_reset_email.html', context)


def password_reset_confirm(request):
    context = {'page_title': 'Password Reset Confirm',
               'main_title': 'Password Reset Confirm ......',
               }
    return render(request, 'registration/password_reset_confirm.html', context)


def password_reset_complete(request):
    context = {'page_title': 'Password Reset Complete',
               'main_title': 'Password Reset Complete ......',
               }
    return render(request, 'registration/password_reset_complete.html', context)


def password_reset_done(request):
    context = {'page_title': 'Password Reset Done',
               'main_title': 'Password Reset Done ......',
               }
    return render(request, 'registration/password_reset_done.html', context)


def login_redirect(request):
    context = {}
    user = request.user
    doctor = Doctor.objects.filter(user_id=user.id)
    patient = Patient.objects.filter(user_id=user.id)
    pharmacist = Pharmacist.objects.filter(user_id=user.id)
    if len(doctor) > 0:
        context['doctor'] = Doctor.objects.get(user_id=user.id)
        return HttpResponseRedirect('/doctor_personal_data_review')
    if len(patient) > 0:
        context['patient'] = Patient.objects.get(user_id=user.id)
        return HttpResponseRedirect('/patient_personal_data_review')
    if len(pharmacist) > 0:
        context['pharmacist'] = Pharmacist.objects.get(user_id=user.id)
        return HttpResponseRedirect('/pharmacist_personal_data_review')

    return HttpResponseRedirect('/password_reset_form')


def admin(request):
    if request.user.is_superuser:
        return HttpResponseRedirect('/admin/')
    else:
        return HttpResponseRedirect(reverse('admin:index'))

#                       Despre 'admin:index' :
# {% block branding %}
# <h1 id="site-name"><a href="{% url 'admin:index' %}">Polls Administration</a></h1>
# {% endblock %}
