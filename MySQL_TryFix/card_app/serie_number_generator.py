""" A class defining a string of non-look-alike digits with a check digit, which can be incremented.

The intent is to create compact serial numbers using a base 32 number system.
* a "host" field can be defined, so that if the serial numbers are issued by a known set of loosely cooperative units
  (think of couchdb hosts) the numbers will not conflict.

The default alphabet can be replaced by any unicode string you wish. The check digit will be from your alphabet.

The string is initialized using a five-argument call like; idstring.IDstring(IDstr, seed, host, seedstore, hash)
:IDstr an already-defined idstring.IDstring with checksum. [or None, if you are supplying a "seed"]
:seed - a "seed" value of valid idstring.ALPHABET characters with no checksum [ignored if IDstr is supplied]
:host - the identity number of the" host" on which it is running. !!! defines the size of the .host field !!!
:seedstore - a call-back function which the caller promises to use to preserve the state of the seed value.
- - - that function will be called with an idstring object so the caller can extract the incremented seed for storage.
- - - REMINDER...use the .value attribute or get_seed() method. The value of the IDstring cannot be incremented!
:hash - additional character(s) to alter the check digit, so that IDstrings for different purposes do not match.
 Copyright 2013, eHealth Africa   http://www.ehealthafrica.org

 """
#  This code is released and licensed under the terms of the Lesser GPL license as specified at the
#  following URL:
#   http://www.gnu.org/copyleft/lgpl.html
#
from __future__ import unicode_literals
import sys
import collections

__author__ = "Vernon Cole <vernondcole@gmail.com>"
__version__ = "1.0.3"

# -- a short calling sample -- real code would use a better storage method ---------
# - import pickle, idstring
# -
# - def my_seedstore(iDstr):   # function to preserve the idString seed
# -    global my_seed_memory
# -    pkl_file = open('mySeedStore,pkl,'wb')
# -    my_seed_memory['seed'] = iDstr.get_seed()
# -    pickle.dump(my_seed_memory,pkl_file,-1)
# -    pkl_file.close()
# -
# - # set up the idString for this run by retrieving it from storage
# - try:  # retrieve the idString seed
# -    pkl_file = open('mySeedStore.pkl','rb')
# -    my_seed_memory = pickle.load(pkl_file)
# -    pkl_file.close()
# - except:
# -    # # the next statement would have been used to initialize the system & set the "host" number
# -    my_seed_memory = {'host':'101','seed':'0000'} # initial value -- to be updated by pickle on later runs
# -    raise Warning('Caution: IDstring storage not found -- your system may be broken')
# -
# - # initialize the idString factory with the saved values
# - present_id = idstring.idString(seed=my_seed_memory['seed'], host=my_seed_memory['host'], my_seedstore)
# -
# - while I_am_still_working:
# -    some_patient_data = some_kind_of_input()
# -    present_id += 1    # generate a new patient number
# -    print('Storing data for %s now...' % repr(present_id))
# -    store_info(str(present_id), some_patient_data)
# -
# -----------------------------------------------------------------------

if sys.version_info[0] > 2:
    basestring = str  # Python3
    text = str
else:  # Python2
    text = unicode


# Define exceptions
class IdStringError(ValueError):
    """Base class for all errors in the IdString package"""
    pass


class OutOfRangeError(IdStringError): pass


class InvalidIdError(IdStringError): pass


class SerieNumberID(text):
    # define the characters which may legally appear in an idString
    numbers = "0123456789"

    # create a list of George Carlan's "the seven words you can't say on T.V."
    # plus a few extras, but ignoring any with an "I" in them, since the default ALPHABET has no "I"
    dirty_words = [c[::-1] for c in ['KCUF', 'TNUC', 'TRAF', 'DRUT', 'TAWT', 'LLAB', 'ETUF', 'ALUP', 'DZIP']]
    # the words are stored backwards in the source, because 'kcuf' is not objectionable.

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
print(len(alphabet))  # 22

def letters_generator(alphabet):
    series = ''
    lung = len(alphabet)
    for i in range(lung):
        for j in range(lung):
            for k in range(lung):  # 26
                series = alphabet[i]+alphabet[j]+alphabet[k]
                # print(series)
                k += 1
            j += 1
        i += 1
    return series

result = letters_generator("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
print(result)



serie = "AAA"
number = "101"
initials = serie + number
try:
    seed_value = function_to_fetch_a_seed(initials)
except someError:
    seed_value = "100"
id = idstring.IDstring(None, seed_value, initials)
print('This id =', id)
id += 1

function_to_store_your_seed(id, initials)


import random

def generate1(unique):
    chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    numb = "1234567890"
    while True:
        chars_list = "".join(random.choice(chars) for _ in range(3))
        numb_list = "".join(random.choice(numb) for _ in range(6))
        final_list = chars_list + numb_list
        if final_list not in unique:
            unique.add(final_list)
            break


unique = set()
for _ in range(1):
    generate1(unique)
print(unique)




import uuid
uuid.uuid4().hex[:8].upper()


import random


def generate2(unique):
    chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    numb = "1234567890"
    # while True:
    for i in range(len(chars)-1):
        chars_list = chars[i] + chars[i+1] + chars[i+2]
        # print(chars_list)
        numb_list = "".join(numb for _ in range(6))
        final_list = chars_list + numb_list
        # i += 1
        if final_list not in unique:
            unique.add(final_list)
            break
        if chars_list not in unique:
            unique.add(chars_list)
            break


unique = set()
for _ in range(10):
    generate2(unique)
print(unique)


chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
for i in range(len(chars) - 1):
    j = 0
    k = 0
    if j == i and k == i:
        print(chars[i] + chars[j] + chars[k])


    # chars_list = chars[i] + chars[i + 1] + chars[i + 2] + chars[i + 3]
    # print(chars_list)


import strgen

strgen.StringGenerator("[\d\w]{10}").render()



first_letter = alphabet[1]
