""""
Resources :
The django-import-export library work with the concept of Resource,
which is class definition very similar to how Django handle model forms and admin classes.

In the documentation the authors suggest to put the code related to the resources inside the admin.py file.
But if the implementation is not related to the Django admin,
I usually prefer to create a new module named resources.py inside the app folder.


'ImportExportModelAdmin' instead of 'resources.ModelResource' to solve the following error :
raise ValueError('Wrapped class must subclass ModelAdmin.')
ValueError: Wrapped class must subclass ModelAdmin.
"""

from import_export import resources  # youtube

from .models import *


class UserResource(resources.ModelResource):  # youtube
    class Meta:
        model = User


# class RoleResource(resources.ModelResource):
#     class Meta:
#         model = Role


class PatientResource(resources.ModelResource):  # youtube
    class Meta:
        model = Patient


class ResidenceResource(resources.ModelResource):
    class Meta:
        model = Residence


class DoctorResource(resources.ModelResource):
    class Meta:
        model = Doctor
        # import_id_fields = ('user', 'authorisation', 'type', 'speciality',)


class DoctorTypeResource(resources.ModelResource):
    class Meta:
        model = DoctorType


# book_resource = resources.modelresource_factory(model=card_app)()
# dataset = tablib.Dataset(['', 'New book'], headers=['id', 'name'])
#
# result = book_resource.import_data(dataset, dry_run=True)
# print(result.has_errors())  # => False
#
# result = book_resource.import_data(dataset, dry_run=False)


class PharmacistResource(resources.ModelResource):  # TODO: Duplicated code fragment (28 lines long).
    # Inspection info:Reports duplicated blocks of code from the selected scope: the same file or the entire project.
    class Meta:
        model = Pharmacist


class SpecialityResource(resources.ModelResource):
    class Meta:
        model = Speciality


class DiagnosticTypeResource(resources.ModelResource):
    class Meta:
        model = DiagnosticType


class ClinicResource(resources.ModelResource):
    class Meta:
        model = Clinic


class ClinicTypeResource(resources.ModelResource):  # TODO: Duplicated code fragment (48 lines long).
    # Inspection info:Reports duplicated blocks of code from the selected scope: the same file or the entire project.
    class Meta:
        model = ClinicType


class ConsultationResource(resources.ModelResource):
    class Meta:
        model = Consultation


class PrescriptionTypeResource(resources.ModelResource):
    class Meta:
        model = PrescriptionType


class ActiveSubstanceResource(resources.ModelResource):
    class Meta:
        model = ActiveSubstance


class PharmaceuticalFormResource(resources.ModelResource):
    class Meta:
        model = PharmaceuticalForm


class ConcentrationResource(resources.ModelResource):
    class Meta:
        model = Concentration


class PrescriptionResource(resources.ModelResource):
    class Meta:
        model = Prescription


class PrescriptionPickUpResource(resources.ModelResource):
    class Meta:
        model = PrescriptionPickUp


class InsuranceResource(resources.ModelResource):
    class Meta:
        model = Insurance


class PharmacyResource(resources.ModelResource):
    class Meta:
        model = Pharmacy
