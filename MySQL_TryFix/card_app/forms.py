from .models import *

from django import forms


class UserData(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)  # NEW - ca sa vada campul ca parola

    class Meta:
        model = User
        fields = '__all__'


# class RoleData(forms.Form):
#     # Reports duplicated blocks of code from the selected scope: the same file or the entire project.'
#     class Meta:
#         model = Role
#         fields = '__all__'


class PatientData(forms.Form):
    # date_of_birth = forms.DateField(input_formats=['%Y-%m-%d'])  # NEW 07.DEC : din pacate insa nu rezolva eroarea de format la import de data
    # sau de tipul:
    # date = forms.DateField(label='buy date', input_formats=['%Y-%m-%d'], initial=date.today)
    # sau de tipul:
    # YourModel.completion_date = your_csv_dict['completion_date'].strftime("%Y-%d-%m")
    class Meta:
        model = Patient
        fields = '__all__'


class ResidenceData(forms.Form):
    class Meta:
        model = Residence
        fields = '__all__'


class DoctorData(forms.ModelForm):  # NEW : cu 'form.Form' nu merge
    class Meta:
        model = Doctor
        fields = '__all__'


class DoctorTypeData(forms.Form):
    class Meta:
        model = DoctorType
        fields = '__all__'


class PharmacistData(forms.Form):  # TODO: 'Duplicated code fragment (29 lines long). Inspection info:
    # Reports duplicated blocks of code from the selected scope: the same file or the entire project.'
    class Meta:
        model = Pharmacist
        fields = '__all__'


class SpecialityData(forms.Form):
    class Meta:
        model = Speciality
        fields = '__all__'


class DiagnosticTypeData(forms.Form):
    class Meta:
        model = DiagnosticType
        fields = '__all__'


class ClinicData(forms.Form):
    class Meta:
        model = Clinic
        fields = '__all__'


class ClinicTypeData(forms.Form):
    class Meta:
        model = ClinicType
        fields = '__all__'


class ConsultationData(forms.Form):
    # date = forms.DateField(widget=DateInput(format='%d %b %Y %H:%M:%S %Z'), input_formats=["%d %b %Y %H:%M:%S %Z"])
    class Meta:
        fields = '__all__'
    #
    # creation_date = forms.DateField(
    #     widget=forms.DateInput(format='%d/%m/%Y'),
    #     input_formats=('%d/%m/%Y',)
    # )


class ConsultationForm(forms.ModelForm):              # NEW
    class Meta:
        model = Consultation
        fields = '__all__'


class PrescriptionTypeData(forms.Form):
    class Meta:
        model = PrescriptionType
        fields = '__all__'


class ActiveSubstanceData(forms.Form):
    class Meta:
        model = ActiveSubstance
        fields = '__all__'


class PharmaceuticalFormData(forms.Form):
    class Meta:
        model = PharmaceuticalForm
        fields = '__all__'


class ConcentrationData(forms.Form):
    class Meta:
        model = Concentration
        fields = '__all__'


class PrescriptionData(forms.Form):
    class Meta:
        model = Prescription
        fields = '__all__'
    #
    # creation_date = forms.DateField(
    #     widget=forms.DateInput(format='%d/%m/%Y'),
    #     input_formats=('%d/%m/%Y',)
    # )


class PrescriptionPickUpData(forms.Form):
    class Meta:
        model = PrescriptionPickUp
        fields = '__all__'
    #
    # pick_up_date = forms.DateField(
    #     widget=forms.DateInput(format='%d/%m/%Y'),
    #     input_formats=('%d/%m/%Y',)
    # )


# protected database column django modelform read only : NEW: 15.DEC
class PrescriptionPickUpForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PrescriptionPickUpForm, self).__init__(*args, **kwargs)
        self.fields['field'].widget.attrs['readonly'] = True

    class Meta:
        model = PrescriptionPickUp


class PharmacyData(forms.Form):
    class Meta:
        model = Pharmacy
        fields = '__all__'


class InsuranceData(forms.Form):
    class Meta:
        model = Insurance
        fields = '__all__'
