# Register your models here.

from django.contrib import admin
# from import_export import fields
from import_export.admin import ImportExportModelAdmin
# from import_export.widgets import FloatWidget

from card_app.models import *

# The following 26 lines are related to the DjangoDB : 'db.sqlite3'
from .resources import DoctorResource

# admin.site.register(User)
# admin.site.register(Role)


admin.site.register(Patient)




admin.site.register(Residence)
admin.site.register(Doctor)
admin.site.register(DoctorType)
admin.site.register(Pharmacist)
admin.site.register(Speciality)

admin.site.register(DiagnosticType)
admin.site.register(Clinic)
admin.site.register(ClinicType)
admin.site.register(Consultation)


admin.site.register(PrescriptionType)
admin.site.register(ActiveSubstance)
admin.site.register(PharmaceuticalForm)
admin.site.register(Concentration)
admin.site.register(Prescription)

admin.site.register(PrescriptionPickUp)

admin.site.register(Insurance)
admin.site.register(Pharmacy)
# admin.site.register(CommonModel)

# admin.site.register(AutoDateTimeField)   # NEW/01.DEC.2020

# AttributeError: 'module' object has no attribute 'META' in Django Project :

# django.contrib.admin.sites.AlreadyRegistered: The model XXXXXX is already registered in app 'card_app' :

admin.site.unregister(User)


# admin.site.unregister(Role)


admin.site.unregister(Patient)
admin.site.unregister(Residence)
admin.site.unregister(Doctor)
admin.site.unregister(DoctorType)
admin.site.unregister(Pharmacist)
admin.site.unregister(Speciality)

admin.site.unregister(DiagnosticType)
admin.site.unregister(Clinic)
admin.site.unregister(ClinicType)


admin.site.unregister(Consultation)

admin.site.unregister(PrescriptionType)
admin.site.unregister(ActiveSubstance)
admin.site.unregister(PharmaceuticalForm)
admin.site.unregister(Concentration)
admin.site.unregister(Prescription)

admin.site.unregister(PrescriptionPickUp)

admin.site.unregister(Insurance)
admin.site.unregister(Pharmacy)

"""
>  >  >  Creating import-export resource <  <  <  
To integrate django-import-export with our Book model , 
we will create a 'ModelResource' class in 'admin.py' that will describe how this resource can be imported or exported :
"""


@admin.register(User)
class UserAdmin(ImportExportModelAdmin):
    pass


# @admin.register(Role)
# class RoleAdmin(ImportExportModelAdmin):
#     pass


@admin.register(Patient)
class PatientAdmin(ImportExportModelAdmin):
    pass


@admin.register(Residence)
class ResidenceAdmin(ImportExportModelAdmin):
    pass


@admin.register(Doctor)
class DoctorAdmin(ImportExportModelAdmin):
    resource_class = DoctorResource


@admin.register(DoctorType)
class DoctorTypeAdmin(ImportExportModelAdmin):
    pass


@admin.register(Pharmacist)
class PharmacistAdmin(ImportExportModelAdmin):
    pass


@admin.register(Speciality)
class SpecialityAdmin(ImportExportModelAdmin):
    pass


@admin.register(DiagnosticType)
class DiagnosticTypeAdmin(ImportExportModelAdmin):
    pass


@admin.register(Clinic)
class ClinicAdmin(ImportExportModelAdmin):
    pass


@admin.register(ClinicType)
class ClinicTypeAdmin(ImportExportModelAdmin):
    pass


@admin.register(Consultation)
class ConsultationAdmin(ImportExportModelAdmin):
    # Consultation = fields.Field(column_name='date_time', widget=FloatWidget)
    # TODO: date_time column is imported like this: '43836.0', and the previous line is not solving it
    # tablib should be a better option ?? : pip install -e git + https://github.com/kennethreitz/tablib.git#egg=tablib
    pass


@admin.register(PrescriptionType)
class PrescriptionTypeAdmin(ImportExportModelAdmin):
    pass


@admin.register(ActiveSubstance)
class ActiveSubstanceAdmin(ImportExportModelAdmin):
    pass


@admin.register(PharmaceuticalForm)
class PharmaceuticalFormAdmin(ImportExportModelAdmin):
    pass


@admin.register(Concentration)
class ConcentrationAdmin(ImportExportModelAdmin):
    pass


@admin.register(Prescription)
class PrescriptionAdmin(ImportExportModelAdmin):
    pass


@admin.register(PrescriptionPickUp)
class PrescriptionPickUpAdmin(ImportExportModelAdmin):
    pass


@admin.register(Insurance)
class InsuranceAdmin(ImportExportModelAdmin):
    pass


@admin.register(Pharmacy)
class PharmacyAdmin(ImportExportModelAdmin):
    pass
