from django.urls import path

from card_app import views

urlpatterns = [
    # DOCTOR related urls :
    # path('doctor_login', views.doctor_login, name='doctor_login'),
    path('doctor_registration', views.doctor_registration, name='doctor_registration'),
    path('doctor_patient_search', views.doctor_patient_search, name='doctor_patient_search'),
    # path('doctor_prescription_by_SSN_review', views.doctor_prescription_by_SSN_review, name='doctor_prescription_by_SSN_review'),
    path('doctor_all_prescriptions_review', views.doctor_all_prescriptions_review, name='doctor_all_prescriptions_review'),
    path('doctor_all_consultations_review', views.doctor_all_consultations_review, name='doctor_all_consultations_review'),
    path('doctor_new_consultation', views.doctor_new_consultation, name='doctor_new_consultation'),
    path('doctor_new_prescription', views.doctor_new_prescription, name='doctor_new_prescription'),
    # path('user_create_view', views.user_create_view, name='user_create_view'),  # NEW
    # path('doctor_create_view', views.doctor_create_view, name='doctor_create_view'),  # NEW
    path('doctor_personal_data_review', views.doctor_personal_data_review, name='doctor_personal_data_review'),  # NEW

    # PATIENT related urls :
    # path('patient_login', views.patient_login, name='patient_login'),
    # path('patient_login_var-curs', views.patient_login, name='patient_login_var-curs'),
    path('patient_registration', views.patient_registration, name='patient_registration'),
    path('patient_personal_data_review', views.patient_personal_data_review, name='patient_personal_data_review'),

    # PHARMACIST related urls :
    # path('pharmacist_login', views.pharmacist_login, name='pharmacist_login'),
    # path('pharmacist_patient_search', views.pharmacist_patient_search, name='pharmacist_patient_search'),
    path('pharmacist_registration', views.pharmacist_registration, name='pharmacist_registration'),
    path('pharmacist_personal_data_review', views.pharmacist_personal_data_review,
         name='pharmacist_personal_data_review'),
    # path('pharmacist_prescription_search', views.pharmacist_prescription_search, name='pharmacist_prescription_search'),
    path('pharmacist_all_prescriptions_review', views.pharmacist_all_prescriptions_review, name='pharmacist_all_prescriptions_review'),


    # HOME url :
    path('', views.virtual_health_card, name='virtual_health_card'),

    # AUTHENTICATION related urls :
    # path('login', views.login_common, name='login'),
    # path('logged_out', views.logged_out, name='logged_out'),
    path('password_reset_form', views.password_reset_form, name='password_reset_form'),
    path('password_reset_email', views.password_reset_email, name='password_reset_email'),
    path('password_reset_confirm', views.password_reset_confirm, name='password_reset_confirm'),
    path('password_reset_complete', views.password_reset_complete, name='password_reset_complete'),
    path('password_reset_done', views.password_reset_done, name='password_reset_done'),
    path('login_redirect', views.login_redirect, name='login_redirect'),

]


