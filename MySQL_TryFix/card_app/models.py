# blank=True,  # TODO: trebuie sa avem aceasta mentiune sau NU :
# blank determines whether the field will be required in forms. This includes the admin and your custom forms.
# If blank=True , then the field will not be required, whereas if it's False the field cannot be blank.

# null=True,  # TODO: trebuie sa avem aceasta mentiune sau NU :
# null=True sets NULL (versus NOT NULL) on the column in your DB.
# Blank values for Django field types such as DateTimeField or ForeignKey will be stored as NULL in the DB.
# https://stackoverflow.com/questions/8609192/differentiate-null-true-blank-true-in-django
import uuid

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class CommonModel(models.Model):                                    # NEW 04.DEC
    created_at = models.DateField(default=timezone.now)  # ?? sau (auto_now_add=True) ??
    updated_at = models.DateField(default=timezone.now)  # ?? sau (auto_now=True) ??
    # hitrate = models.IntegerField(default=0)

    class Meta:
        abstract = True


class Patient(CommonModel):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
    )
    ssn = models.IntegerField()
    date_of_birth = models.DateField(null=True, blank=True)  # TODO: trebuie 'null=True, blank=True' ??
    # It seems that django's DateField is accepting strings as a default. Try converting your datetime field like this:
    # YourModel.date_of_birth = your_csv_dict['completion_date'].strftime("%Y-%d-%m")      # NEW 08.DEC
    citizenship = models.TextField()
    phone_nr = models.TextField()
    residence = models.ForeignKey(  # One-To-Many relation to Residences table
        'Residence',
        on_delete=models.DO_NOTHING,
    )
    insurance = models.ForeignKey(  # One-To-Many relation to Insurances table
        'Insurance',
        on_delete=models.DO_NOTHING,
    )
    doctor = models.ForeignKey(  # The field 'doctor' clashes with the field 'doctor' from model 'card_app.commonmodel'.
        'Doctor',
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return str(self.id) + ". " + self.user.first_name + " " + self.user.last_name + " (" + self.residence.name + ")"

    class Meta:
        ordering = ('id',)


class Residence(models.Model):
    name = models.TextField()

    def __str__(self):
        return str(self.id) + ". " + self.name

    class Meta:
        ordering = ('id',)


class Doctor(CommonModel):  # class Doctor(CommonModel, models.Model)
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
    )
    authorisation = models.IntegerField()     # CharField(max_length=5)
    doctor_type = models.ForeignKey(
        'DoctorType',
        on_delete=models.DO_NOTHING,
    )
    speciality = models.ForeignKey(
        'Speciality',
        on_delete=models.DO_NOTHING,
    )
    clinic = models.ForeignKey(
        'Clinic',
        null=True,             # NEW 07.DEC - nu mergea importul in BD
        blank=True,             # NEW 07.DEC - nu mergea importul in BD
        on_delete=models.DO_NOTHING,
    )
    clinic_type = models.ForeignKey(
        'ClinicType',
        null=True,             # NEW 07.DEC - nu mergea importul in BD
        blank=True,             # NEW 07.DEC - nu mergea importul in BD
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return str(self.id) + ". " + self.user.first_name + " " + self.user.last_name
               # + \
               # " (" + self.clinic.name + "--" + self.clinic_type.clinic_type + "--" + self.speciality.name + ")"

    class Meta:
        ordering = ('id',)


class DoctorType(models.Model):
    doctor_type = models.TextField()

    def __str__(self):
        return str(self.id) + ". " + self.doctor_type

    class Meta:
        ordering = ('id',)


class Pharmacist(CommonModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    authorisation = models.CharField(max_length=6)
    pharmacy = models.ForeignKey(
        'Pharmacy',
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return str(self.id) + ". " + self.user.first_name + " " + self.user.last_name + " (" + self.pharmacy.name + ")"

    class Meta:
        ordering = ('id',)


class Speciality(models.Model):
    name = models.TextField()

    def __str__(self):
        return str(self.id) + ". " + self.name

    class Meta:
        ordering = ('id',)


class DiagnosticType(models.Model):
    diag_type = models.TextField()

    def __str__(self):
        return str(self.id) + ". " + self.diag_type

    class Meta:
        ordering = ('id',)


class Clinic(models.Model):
    name = models.TextField(
    )

    def __str__(self):
        return str(self.id) + ". " + self.name

    class Meta:
        ordering = ('id',)


class ClinicType(models.Model):
    clinic_type = models.TextField()

    def __str__(self):
        return str(self.id) + ". " + self.clinic_type

    class Meta:
        ordering = ('id',)


class Consultation(CommonModel):
    patient = models.ForeignKey(  # The field 'patient' clashes with the field 'patient' from model 'card_app.commonmodel'.
        'Patient',
        on_delete=models.CASCADE
    )
    diagnostic_type = models.ForeignKey(
        'DiagnosticType',
        on_delete=models.DO_NOTHING,
    )
    medical_report = models.TextField()

    doctor = models.ForeignKey(  # The field 'doctor' clashes with the field 'doctor' from model 'card_app.commonmodel'.
        'Doctor',
        on_delete=models.DO_NOTHING,
    )
    clinic = models.ForeignKey(
        'Clinic',
        on_delete=models.DO_NOTHING,
    )
    clinic_type = models.ForeignKey(
        'ClinicType',
        on_delete=models.DO_NOTHING,
    )
    insurance = models.ForeignKey(
        'Insurance',
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return str(self.id) + ". " + str(self.created_at) + " - " + self.patient.user.first_name + " " + \
               self.patient.user.last_name + " - " + " (" + str(self.clinic) + " - " + \
               self.doctor.speciality.name + ")"
        # when concatenate, all the fields are taking the type of the first(this is why is mentioned str(self.date) )
        # since there is no doctor speciality in Consultations, this is imported from Doctors table: doctor.speciality,
        # si apoi facem legatura cu tabela Speciality, ca sa luam numele specialitatii: doctor.speciality.name

        # return str(self.id) + ". " + str(self.creation_date) + " - " + self.medical_report  # NEW
    class Meta:
        ordering = ('id',)


class PrescriptionType(models.Model):
    diagnostic_code = models.CharField(max_length=10)
    prescription_type = models.TextField()

    def __str__(self):
        return str(self.id) + ". " + self.diagnostic_code + " / " + self.prescription_type

    class Meta:
        ordering = ('id',)


class ActiveSubstance(models.Model):
    active_substance = models.TextField()

    def __str__(self):
        return str(self.id) + ". " + self.active_substance

    class Meta:
        ordering = ('id',)


class PharmaceuticalForm(models.Model):
    pharmaceutical_form = models.TextField()

    def __str__(self):
        return str(self.id) + ". " + self.pharmaceutical_form

    class Meta:
        ordering = ('id',)


class Concentration(models.Model):
    concentration = models.TextField()

    def __str__(self):
        return str(self.id) + ". " + str(self.concentration)

    class Meta:
        ordering = ('id',)


class Prescription(CommonModel):
    series = models.CharField(max_length=3, primary_key=True)  # NEW: 15.DEC

    number = models.CharField(max_length=6, default=uuid.uuid4, unique=True)  # NEW: 15.DEC

    active_substance = models.ForeignKey(   # One-To-Many relation to ActiveSubstance table
        'ActiveSubstance',
        on_delete=models.DO_NOTHING,
    )
    pharmaceutical_form = models.ForeignKey(  # One-To-Many relation to PharmaceuticalForm table
        'PharmaceuticalForm',
        on_delete=models.DO_NOTHING,
    )
    concentration = models.ForeignKey(      # One-To-Many relation to Concentration table
        'Concentration',
        on_delete=models.DO_NOTHING,
    )
    consultation = models.ForeignKey(  # The field 'consultation' clashes with the field 'consultation' from model 'card_app.commonmodel'.
        'Consultation',
        on_delete=models.DO_NOTHING,
    )
    prescription_type = models.ForeignKey(  # One-To-Many relation to PrescriptionType table  # dublare cu linia 241
        'PrescriptionType',
        on_delete=models.DO_NOTHING,
    )
    doctor = models.ForeignKey(  # The field 'doctor' clashes with the field 'doctor' from model 'card_app.commonmodel'.
        'Doctor',
        on_delete=models.DO_NOTHING,
    )
    patient = models.ForeignKey(  # The field 'patient' clashes with the field 'patient' from model 'card_app.commonmodel'.
        'Patient',
        on_delete=models.DO_NOTHING,
    )
    insurance = models.ForeignKey(      # One-To-Many relation to Insurance table
        'Insurance',
        on_delete=models.DO_NOTHING,
    )
    # updated_at = models.AutoDateTimeField()  # NEW/01.DEC.2020                            # commented on 08.DEC
    # # AutoDateTimeField() takes no arguments: "updated_at = models.AutoDateTimeField(default=timezone.now)"

    def __str__(self):
        return str(self.id) + ". " + self.doctor.user.first_name + " " + self.doctor.user.last_name + \
               " " + self.doctor.speciality.name + " (Picked-up by " + \
               self.patient.user.first_name + " " + self.patient.user.last_name + ")"

    class Meta:
        ordering = ('id',)


class PrescriptionPickUp(CommonModel):
    pharmacist = models.ForeignKey(  # The field 'pharmacist' clashes with the field 'pharmacist' from model 'card_app.commonmodel'.
        'Pharmacist',
        on_delete=models.DO_NOTHING,
    )
    pharmacy = models.ForeignKey(      # One-To-Many relation to Pharmacies table
        'Pharmacy',
        on_delete=models.DO_NOTHING,
    )
    prescription = models.ForeignKey(  # The field 'prescription' clashes with the field 'prescription' from model 'card_app.commonmodel'.
        'Prescription',
        on_delete=models.DO_NOTHING,
    )
    patient = models.ForeignKey(  # The field 'patient' clashes with the field 'patient' from model 'card_app.commonmodel'.
        'Patient',
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return str(self.id) + ". " + str(self.patient) + ". " + str(self.created_at)

    class Meta:
        ordering = ('id',)


class Insurance(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.id) + ". " + self.name

    class Meta:
        ordering = ('id',)


class Pharmacy(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.id) + ". " + self.name

    class Meta:
        ordering = ('id',)

