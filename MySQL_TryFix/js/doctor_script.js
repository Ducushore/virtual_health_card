// function startTime() {
//     var today = new Date();
//     var h = today.getHours();
//     var m = today.getMinutes();
//     var s = today.getSeconds();
//     m = checkTime(m);
//     s = checkTime(s);
//     document.getElementById('clockDisplay').innerHTML = h + ":" + m + ":" + s;
//     var t = setTimeout(startTime, 1000);
// }
//
// function checkTime(i) {
//     if (i < 10) {
//         i = "0" + i
//     }
//     ;  // add zero in front of numbers < 10
//     return i;
// }
//
// startTime()  // FOARTE IMPORTANTA aceasta linie de apelare a functiei


function startTime() {

    // Date
    var mydate = new Date();
    var year = mydate.getFullYear();
        if (year < 1000) {
            year += 1900
        }
    var day = mydate.getDay();
    var month = mydate.getMonth();
    var daym = mydate.getDate();

    // var dayarray = ["Sun, ", "Mon, ", "Tue, ", "Wed, ", "Thu, ", "Fri, ", "Sat, "];
    //am inlocuit "new Aray("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")" cu sugestia PyCharm
    // var montharray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    // am inlocuit "new Aray("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")" cu sugestia PyCharm

    var dayarray = new Array("Sunday, ", "Monday, ", "Tuesday, ", "Wednesday, ", "Thursday, ", "Friday, ", "Saturday, ");
    var montharray = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "October", "November", "December");

    // Date End

    // Time
    var currentTime = new Date();
    var h = currentTime.getHours();
    var m = currentTime.getMinutes();
    var s = currentTime.getSeconds();
        if (h == 24) {
            h = 0;
        }
        else if (h > 12) {
            h = h - "0";
        }

        if (h < 10) {
            h = "0" + h;
        }

        if (m < 10) {
            m = "0" + m;
        }

        if (s < 10) {
            s = "0" + s;
        }

    var myClock = document.getElementById("clockDisplay")

    // myClock.textContent = "" +dayarray[day]+ " " +daym+ " " +montharray[month]+ " " +year+ " | " +h+ ":" +m+ ":" +s;
    myClock.innerHTML = dayarray[day] + " " +daym+ " " +montharray[month]+ " " +year+ " | " +h+ ":" +m+ ":" +s;

    var t = setTimeout("startTime()", 1000); // every 1000ms = 1 sec the function is triggered
}

startTime()

// Take away the textcontent line and repalce the innertext with this:
// myClock.innerHTML = dayarray[day] + " " +daym+ " " +montharray[month]+ " " +year+ " | " +h+ ":" +m+ ":" +s;

// You need to remove the two lines "myClock.textContent" and "myClock.innerText"
// since they are same lines. You should just write : "clockDisplay.innerHTML= ...."
