from django.contrib.auth.models import User
from django.http import HttpResponse  # youtube
from django.http import HttpResponseRedirect
from django.shortcuts import render

from card_app import models
# DOCTORS' VIEWS :
from card_app.models import Doctor, Speciality, Patient, Pharmacist, Pharmacy, Insurance
from .resources import PatientResource  # youtube


# from tablib import Dataset                  # youtube


# def doctor_login(request):  # pagina de log-in doctor
#     context = {'page_title': 'Doctor Log-In',
#                'main_title': 'DOCTOR Log-In......',
#                'sub_title_1': "To log-in, please fill-in the following fields :"
#                }
#     return render(request, 'doctor/doctor_login.html', context)
# {'form': context} instead of context


def doctor_patient_search(request):
    context = {'patients': models.Patient.objects.all(),  # toti pacientii au fost trimisi catre front-end
               'prescriptions': models.Prescription.objects.all(),  # toate presciptions trimise catre front-end
               'page_title': "Patient Search",
               'main_title': "Doctor's PATIENT-SEARCH Page",
               }
    # filtrarile pot fi facute pe front-end sau pe back-end
    # all furnizeaza toti pacientii, dar 'filter( . . .)' numai un anumit pacient, functie de conditie
    return render(request, 'doctor/doctor_patient_search.html', context)
    # {'form': context} instead of context


def doctor_consultation_review(request):
    context = {'consultations': models.Consultation.objects.all(),
               'page_title': 'Consultation Review',
               'main_title': 'CONSULTATION(S) REVIEW Page',
               }
    return render(request, 'doctor/doctor_consultation_review.html', context)


def doctor_prescription_review(request):
    context = {'prescriptions': models.Prescription.objects.all(),
               'page_title': 'Prescription Review',
               'main_title': 'PRESCRIPTION(S) REVIEW Page',
               }
    return render(request, 'doctor/doctor_prescription_review.html', context)


def doctor_new_consultation_prescription(request):
    context = {'page_title': 'New Consultation/Prescription',
               'main_title': 'NEW CONSULTATION and PRESCRIPTION Page',
               'sub_title_1': "NEW CONSULTATION",
               'sub_title_2': "NEW PRESCRIPTION",
               'sub_title_3': "    => Fill-in all the necessary data and then press the SAVE button :",
               }
    return render(request, 'doctor/doctor_new_consultation_prescription.html', context)
    # {'form': context} instead of context


def doctor_registration(request):
    context = {'page_title': 'New Doctor',
               'main_title': 'New DOCTOR......',
               'sub_title_1': "For registration, please fill the following fields :",
               'footer': "(*) These fields are mandatory !",
               }
    if request.method == "POST":  # TODO: metoda asta se apeleaza cand userul apasa pe butonul de submit.
        print(request.POST)

        # Create user if not exist
        email = request.POST.get('dr_email', '')
        first_name = request.POST.get('dr_f_name', '')
        last_name = request.POST.get('dr_l_name', '')
        password = request.POST.get('password', '')

        existing_users = User.objects.filter(email=email)
        if len(existing_users) > 0:
            user = User.objects.get(email=email)
        else:
            user = User.objects.create_user(username=email, password=password, email=email, first_name=first_name,
                                            last_name=last_name, is_active=True)

        # Create DOCTOR profile
        authorization = request.POST.get('auth_no', '')
        type_ = request.POST.get('dr_type', '')
        speciality_id = request.POST.get('speciality_id', '')

        existing_doctor = Doctor.objects.filter(user=user)
        if len(existing_doctor) > 0:
            doctor = Doctor.objects.get(user=user)
        else:
            doctor = Doctor.objects.create(user=user, authorisation=authorization, speciality_id=speciality_id,
                                           type=type_)

        return HttpResponseRedirect('/')

    if request.method == "GET":  # TODO: de obicei, metoda asta se apeleaza cand dai ENTER pe un URL in browser.
        # Attach existing user details if present, boxes should become greyed out
        print(request.GET)
        context['specialities'] = Speciality.objects.all()
        return render(request, 'doctor/doctor_registration.html', context)


# PHARMACISTS' VIEWS :

# def pharmacist_login(request):  # pagina de lucru a farmacistului-ului
#     context = {'page_title': 'Pharmacist Log-In',
#                'main_title': 'PHARMACIST Log-In......',
#                'sub_title_1': "To log-in, please fill-in the following fields :"
#                }
#     return render(request, 'pharmacist/pharmacist_login.html', context)
# {'form': context} instead of context


def pharmacist_patient_search(request):
    context = {'patients': models.Patient.objects.all(),  # toti pacientii au fost trimisi catre front-end
               'prescriptions': models.Prescription.objects.all(),  # toate presciptions trimise catre front-end
               'page_title': 'PATIENT Search',
               'main_title': "Pharmacist's PATIENT-SEARCH Page",
               'sub_title_1': "Please fill-in data for at least one of the fields, and then press the SEARCH button :",
               }
    # filtrarile pot fi facute pe front-end sau pe back-end
    # all furnizeaza toti pacientii, dar 'filter( . . .)' numai un anumit pacient, functie de conditie

    return render(request, 'pharmacist/pharmacist_patient_search.html', context)
    # inlocuit "context" cu {'form': context} instead of context


# def pharmacist_prescription_search(request):
#     context = {'patients': models.Patient.objects.all(),  # toti pacientii au fost trimisi catre front-end
#                'prescriptions': models.Prescription.objects.all(),  # toate presciptions trimise catre front-end
#                'page_title': 'PRESCRIPTION Search',
#                'main_title': "Pharmacist's PRESCRIPTION-SEARCH Page",               
#                'sub_title_1': "Please fill-in data for at least one of the fields, and then press the SEARCH button :",
#                'sub_title_2': "",
#                }
#     # filtrarile pot fi facute pe front-end sau pe back-end
#     # all furnizeaza toti pacientii, dar 'filter( . . .)' numai un anumit pacient, functie de conditie
# 
#     return render(request, 'pharmacist/pharmacist_prescription_search.html', context)
#     # inlocuit "context" cu {'form': context} instead of context


def pharmacist_registration(request):
    context = {'page_title': 'New Pharmacist',
               'main_title': 'New PHARMACIST......',
               'sub_title_1': "For registration, please fill-in the following fields :",
               'footer': "(*) These fields are mandatory !",
               }
    if request.method == "POST":  # TODO: metoda asta se apeleaza cand userul apasa pe butonul de submit.
        print(request.POST)

        # Create user if not exist
        email = request.POST.get('ph_email', '')
        first_name = request.POST.get('ph_f_name', '')
        last_name = request.POST.get('ph_l_name', '')
        password = request.POST.get('password', '')

        existing_users = User.objects.filter(email=email)
        if len(existing_users) > 0:
            user = User.objects.get(email=email)
        else:
            user = User.objects.create_user(username=email, password=password, email=email, first_name=first_name,
                                            last_name=last_name, is_active=True)

        # Create Pharmacist profile
        authorization = request.POST.get('auth_no', '')
        pharmacy_id = request.POST.get('pharmacy_id', '')

        existing_pharmacist = Pharmacist.objects.filter(user=user)
        if len(existing_pharmacist) > 0:
            pharmacist = Pharmacist.objects.get(user=user)
        else:
            pharmacist = Pharmacist.objects.create(user=user, authorisation=authorization, pharmacy_id=pharmacy_id)

        return HttpResponseRedirect('/')

    if request.method == "GET":  # TODO: de obicei, metoda asta se apeleaza cand dai ENTER pe un URL in browser.
        # Attach existing user details if present, boxes should become greyed out
        print(request.GET)
        context['pharmacies'] = Pharmacy.objects.all()
        return render(request, 'pharmacist/pharmacist_registration.html', context)


# PACIENTS' VIEWS :

# def patient_login(request):
#     context = {'page_title': 'Patient Log-In',
#                'main_title': 'PATIENT Log-In......',
#                'sub_title_1': "To log-in, please fill-in the following fields :"
#                }
#     if request.method == "POST":  # post pe log-in button
#         # print(request.POST)  # TODO: NU UITA SA-L STERGI APOI
#         # https://docs.djangoproject.com/en/dev/topics/auth/default/#how-to-log-a-user-in - How to log a user in :
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(request, username=username, password=password)
#         if user is not None:
#             login(request, user)
#             return reverse_lazy('patient_personal_data_review')
#         else:
#             return reverse_lazy('virtual_health_card')
#
#     if request.method == "GET":
#         return render(request, 'patient/patient_login_var-curs.html', context)


def patient_registration(request):
    context = {'page_title': 'New Patient',
               'main_title': 'New PATIENT......',
               'sub_title_1': "For registration, please fill-in the following fields :",
               'footer': "(*) These fields are mandatory !",
               }
    if request.method == "POST":  # TODO: metoda asta se apeleaza cand userul apasa pe butonul de submit.
        print(request.POST)

        # Create user if not exist
        email = request.POST.get('pt_email', '')
        first_name = request.POST.get('pt_f_name', '')
        last_name = request.POST.get('pt_l_name', '')
        password = request.POST.get('password', '')

        existing_users = User.objects.filter(email=email)
        if len(existing_users) > 0:
            user = User.objects.get(email=email)
        else:
            user = User.objects.create_user(username=email, password=password, email=email, first_name=first_name,
                                            last_name=last_name, is_active=True)

        # Create PATIENT profile
        ssn = request.POST.get('SSN', '')
        phone_nr = request.POST.get('phone_nr', '')
        date_of_birth = request.POST.get('date_of_birth', '')
        residence = request.POST.get('residence', '')
        insurance_id = request.POST.get('insurance_id', '')
        general_practitioner_id = request.POST.get('general_practitioner_id', '')

        existing_patient = Patient.objects.filter(user=user)
        if len(existing_patient) > 0:
            patient = Patient.objects.get(user=user)
        else:
            patient = Patient.objects.create(user=user, SSN=SSN, phone_nr=phone_nr, insurance_id=insurance_id)

        return HttpResponseRedirect('/')

    if request.method == "GET":  # TODO: de obicei, metoda asta se apeleaza cand dai ENTER pe un URL in browser.
        # Attach existing user details if present, boxes should become greyed out
        print(request.GET)
        context['insurance'] = Insurance.objects.all()
        return render(request, 'patient/patient_registration.html', context)


def patient_personal_data_review(request):
    current_user = request.user
    context = {"current_user": current_user,
               'page_title': "Patient's Page",
               'main_title': "Patient's PERSONAL DATA-REVIEW Page",
               }
    return render(request, 'patient/patient_personal_data_review.html', context)


# HOME PAGE :
def virtual_health_card(request):
    context = {'page_title': 'Virtual Health Card Application',
               'main_title': 'Virtual Health Card Application',
               }
    return render(request, 'virtual_health_card.html', context)


# AUTHENTICATION VIEWS :
def logged_out(request):
    context = {'page_title': 'logged out',
               'main_title': 'Logged-Out......',
               }
    return render(request, 'virtual_health_card.html', context)


def password_reset_form(request):
    context = {'page_title': 'Password Reset Form',
               'main_title': 'Password Reset Form ......',
               'sub_title_1': "To reset Password, please follow the process :"
               }
    return render(request, 'registration/password_reset_form.html', context)


def password_reset_email(request):
    context = {'page_title': 'Password Reset email',
               'main_title': 'Password Reset email ......',
               }
    return render(request, 'registration/password_reset_email.html', context)


def password_reset_confirm(request):
    context = {'page_title': 'Password Reset Confirm',
               'main_title': 'Password Reset Confirm ......',
               }
    return render(request, 'registration/password_reset_confirm.html', context)


def password_reset_complete(request):
    context = {'page_title': 'Password Reset Complete',
               'main_title': 'Password Reset Complete ......',
               }
    return render(request, 'registration/password_reset_complete.html', context)


def password_reset_done(request):
    context = {'page_title': 'Password Reset Done',
               'main_title': 'Password Reset Done ......',
               }
    return render(request, 'registration/password_reset_done.html', context)


def login_redirect(request):
    context = {}
    user = request.user
    doctor = Doctor.objects.filter(user_id=user.id)
    patient = Patient.objects.filter(user_id=user.id)
    pharmacist = Pharmacist.objects.filter(user_id=user.id)
    if len(doctor) > 0:
        context['doctor'] = Doctor.objects.get(user_id=user.id)
        return HttpResponseRedirect('/doctor_patient_search')
    if len(patient) > 0:
        context['patient'] = Patient.objects.get(user_id=user.id)
        return HttpResponseRedirect('/patient_personal_data_review')
    if len(pharmacist) > 0:
        context['pharmacist'] = Pharmacist.objects.get(user_id=user.id)
        return HttpResponseRedirect('/pharmacist_patient_search')
    return HttpResponseRedirect('/password_reset_form')


# IMPORT - EXPORT , EXCEL - MySQL                      # youtube

def export(request):
    patient_resource = PatientResource()
    dataset = patient_resource.export()
    response = HttpResponse(dataset.xlsx, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="patients.xlsx"'
    return response

# def simple_upload(request):  # youtube
#     # If this is a POST request we need to process the form data :
#     if request.method == 'POST':  # TODO : AttributeError: module 'django.http.request' has no attribute 'method'
#         # I fixed the issue by creating a separate python sheet ????
#
#         patient_resource = PatientResource()
#         dataset = patient_resource.export()  # changed from dataset = Dataset()
#         new_patient = request.FILES['myfile']
#
#         if not new_patient.name.endswitch('xlsx'):
#             messages.info(request, 'wrong format')
#             return render(request, 'import_export/upload.html')
#
#         imported_data = dataset.load(new_patient.read(), format='xlsx')
#         # print(imported_data)
#         for data in imported_data:
#             value = Patient(
#                 data[0],
#                 data[1],
#                 data[2],
#                 data[3],
#                 data[4],
#                 data[5],
#                 data[6],
#                 data[7],
#                 data[8],
#                 data[9],
#                 data[10],
#                 data[11],
#                 data[12],
#             )
#             value.save()
#         # result = patient_resource.import_data(dataset, dry_run=True)  # Test the data import
#
#         # if not result.has_errors():
#         #    patient_resource.import_data(dataset, dry_run=False)  # Actually import now
#     return render(request, 'import_export/upload.html')
